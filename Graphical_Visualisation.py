# Let us import the Libraries required.
import cv2
import numpy as np
# import matplotlib.pyplot as plt
from model import FacialExpressionModel
# import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
# import matplotlib.animation as animation
plt.style.use('ggplot')
# Creating an instance of the class with the parameters as model and its weights.
test_model = FacialExpressionModel("model.json", "model_weights.h5")

# Loading the classifier from the file.
facec = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

def Emotion_Analysis():
    cap = cv2.VideoCapture(0)
    index=0
    graph=[]
    size = 20
    x_vec = np.linspace(0,1,size+1)[0:-1]
    y_vec = np.random.randint(4,size=len(x_vec))
    # x_vec=np.array([-1,0,1,2,3,4,5,6,7,8,9,10])
    # y_vec=np.array([-1,-1,2,1,1,1,1,1,1,1,1,1])
    print(x_vec)
    print(y_vec)
    # exit()
    while (True):
        index+=1
        emotion=None
        # Capture frame-by-frame
        ret, image = cap.read()
        gray_frame = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Image size is reduced by 30% at each image scale.
        scaleFactor = 1.3

        # 5 neighbors should be present for each rectangle to be retained.
        minNeighbors = 5

        # Detect the Faces in the given Image and store it in faces.
        faces = facec.detectMultiScale(gray_frame, scaleFactor, minNeighbors)

        # When Classifier could not detect any Face.
        # if len(faces) == 0:
        #     return [img]

        for (x, y, w, h) in faces:

            # Taking the Face part in the Image as Region of Interest.
            roi = gray_frame[y:y+h, x:x+w]

            # Let us resize the Image accordingly to use pretrained model.
            roi = cv2.resize(roi, (48, 48))

            EMOTIONS = [3,3,3,1,2,3,1]
            #1 happy,2 neutral, 3 Sad
            # Finding the Probability of each Emotion
            preds = test_model.return_probabs(roi[np.newaxis, :, :, np.newaxis])

            # Converting the array into list
            data = preds.tolist()[0]
            # print(data)
            # print(np.argmax(data))
            print(EMOTIONS[np.argmax(data)],data[np.argmax(data)])
            emotion=EMOTIONS[np.argmax(data)]
            # print()
        if emotion is None:
            emotion=0
        # emotion+=1
        print(emotion)
        y_vec[-1] = emotion
        # time_list.append(index)
        graph = live_plotter(x_vec, y_vec,graph)
        y_vec = np.append(y_vec[1:], 0.0)
    cap.release()
    cv2.destroyAllWindows()
#

def live_plotter(x_vec, y1_data, line1, identifier='', pause_time=0.1):
    if line1 == []:
        # this is the call to matplotlib that allows dynamic plotting
        plt.ion()
        fig = plt.figure(figsize=(13, 6))
        ax = fig.add_subplot(111)
        # ax.set_xlim(0, 3)
        # ax.set_ylim(0, 3)
        # create a variable for the line so we can later update it
        # plt.yticks(np.arange(0, 4, 1))
        plt.yticks([0,1,2,3],["No Face","Happy","Neutral","Sad"])
        line1, = ax.plot(x_vec, y1_data, '-o', alpha=0.8)
        # update plot label/title
        plt.ylabel('Facial Emotion')
        plt.title('Title: {}'.format("Emotion Graph"))
        plt.show()

    # after the figure, axis, and line are created, we only need to update the y-data
    line1.set_ydata(y1_data)
    # adjust limits if new data goes beyond bounds
    # if np.min(y1_data) <= line1.axes.get_ylim()[0] or np.max(y1_data) >= line1.axes.get_ylim()[1]:
    #     plt.ylim([np.min(y1_data) - np.std(y1_data), np.max(y1_data) + np.std(y1_data)])
    # this pauses the data so the figure/axis can catch up - the amount of pause can be altered above
    plt.pause(pause_time)

    # return line so we can update it again in the next iteration
    return line1

Emotion_Analysis()